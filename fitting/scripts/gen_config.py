###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("--output", action = "store", dest = "output", type = str, required = True)

parser.add_argument("--decay", action = "store", dest = "decay", type = str, nargs=2, required = True)
parser.add_argument("--fit-type", action = "store", dest = "fit_type", type = str, required = True)
parser.add_argument("--meson", action = "store", dest = "meson", type = str, required = True)

args = parser.parse_args()

assert args.fit_type == "MC" or args.fit_type == "data", f"Unrecognised fit type: {args.fit_type}"

def get_entry(entry, B_decay, D_decay, meson):
    channels = {
        "B2DPi"   : {
            "Ds2KKpi" : {
                "Bs" : {
                    "channel" : r"#it{m}(D^{#minus}[K#pi[as K]#pi]#pi^{#plus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                },
                "Ds" : {
                    "channel" : r"#it{m}(K^{#minus} #pi^{#plus}[as K^{#plus}] #pi^{#minus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                }
            }
        },
        "B2DsPi"  : {
            "Ds2KKpi" : {
                "Bs" : {
                    "channel" : r"#it{m}(D_{s}[KK#pi]#pi)",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                },
                "Ds" : {
                    "channel" :r"#it{m}(K^{#minus} K^{#plus} #pi^{#minus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                }
            },
            "Ds2pipipi" : {
                "Bs" : r"#it{m}(D_{s}[#pi#pi#pi]#pi)",
                "Ds" : r"(#pi^{#minus} #pi^{#plus} #pi^{#minus})",
            }
        },
        "Bs2DsK" : {
            "Ds2KKpi" : {
                "Bs" : {
                    "channel" : r"#it{m}(D_{s}[KK#pi]K[as #pi])",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                },
                "Ds" : {
                    "channel" : r"#it{m}(K^{#minus} K^{#plus} #pi^{#minus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                }
            },
            "Ds2pipipi" : {
                "Bs" : {
                    "channel" : r"#it{m}(D_{s}[#pi#pi#pi]K[as #pi])",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                },
                "Ds" : {
                    "channel" : r"#it{m}(#pi^{#minus} #pi^{#plus} #pi^{#minus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                }
            }
        },
        "Bs2DsPi"  : {
            "Ds2KKpi" : {
                "Bs" : {
                    "channel" : r"#it{m}(D_{s}[KK#pi]#pi)",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                },
                "Ds" : {
                    "channel" : r"#it{m}(K^{#minus} K^{#plus} #pi^{#minus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                }
            },
            "Ds2pipipi" : {
                "Bs" : {
                    "channel" : r"#it{m}(D_{s}[#pi#pi#pi]#pi)",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                },
                "Ds" : {
                    "channel" : r"#it{m}(#pi^{#minus} #pi^{#plus} #pi^{#minus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                }
            }
        },
        "Bs2DsstPi"  : {
            "Ds2KKpi" : {
                "Bs" : {
                    "channel" : r"#it{m}(D_{s}^{*}[KK#pi]#pi)",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                },
                "Ds" : {
                    "channel" : r"#it{m}(K^{#minus} K^{#plus} #pi^{#minus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                }
            }
        },
        "Lb2LcPi"  : {
            "Ds2KKpi" : {
                "Bs" : {
                    "channel" : r"#it{m}(#Lambda_{c}[p[as K]K#pi#pi)",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                },
                "Ds" : {
                    "channel" : r"#it{m}(K^{#minus} K^{#plus} #pi^{#minus})",
                    "B_mass" : "[5280, 5800]",
                    "D_mass" : "[1920, 2015]"
                }
            }
        }
    }
    return channels[B_decay][D_decay][meson][entry]


def write_MC_setup(outfile, args):
    outstring = '{\n'
    if args.decay[0] == "B2DPi":
        outstring += '    "decay_tree" : "Bd2DPiOfflineTree/DecayTree",\n'
    else:
        outstring += '    "decay_tree" : "Bs2DsPiOfflineTree/DecayTree",\n'
    outstring += f'    "B_channel" : "{args.decay[0]}",\n'
    outstring += f'    "B_channel_label" : "{get_entry("channel",args.decay[0], args.decay[1], "Bs")}",\n'
    outstring += f'    "D_channel" : "{args.decay[1]}",\n'
    outstring += f'    "D_channel_label" : "{get_entry("channel",args.decay[0], args.decay[1], "Ds")}",\n'
    outstring += f'    "mode" : "MC",\n'
    outstring += f'    "meson" : "{args.meson}",\n'
    outstring += f'    "B_mass" : {get_entry("B_mass",args.decay[0], args.decay[1], args.meson)},\n'
    outstring += f'    "D_mass" : {get_entry("D_mass",args.decay[0], args.decay[1], args.meson)}\n'
    outstring += '}'

    return outfile.write(outstring)

def write_data_setup(outfile, args):
    outstring = '{\n'
    outstring += '    "decay_tree" : "Bs2DsPiOfflineTree/DecayTree",\n'
    outstring += f'    "B_channel" : "{args.decay[0]}",\n'
    outstring += f'    "B_channel_label" : "{get_entry("channel",args.decay[0], args.decay[1], "Bs")}",\n'
    outstring += f'    "D_channel" : "{args.decay[1]}",\n'
    outstring += f'    "D_channel_label" : "{get_entry("channel",args.decay[0], args.decay[1], "Ds")}",\n'
    outstring += f'    "mode" : "{args.meson}",\n'
    if args.meson == "2D":
        outstring += f'    "meson" : "BsDs",\n'
    elif "split_" in args.meson:
        outstring += f'    "meson" : "{args.meson.replace("split_","")}",\n'
    else:
        outstring += f'    "meson" : "Bs",\n'
    outstring += f'    "B_mass" : [5280, 5800],\n'
    outstring += f'    "D_mass" : [1920, 2015]'
    if args.meson == "combined" or args.meson == "2D":
        outstring += f',\n    "unblinded" : true'
    outstring += '\n}'

    return outfile.write(outstring)

with open(args.output, 'w') as outfile:
    write_MC_setup(outfile, args) if args.fit_type == "MC" else write_data_setup(outfile, args)