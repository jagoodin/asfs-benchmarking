###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
"""

from numba import njit
from numpy import abs as absolute
from typing import Dict, List, Union

def parse_cut(variable: str,
              value:    Union[List[float], float, bool],
              category: str,
              temp_var: bool=False
    ):
    """
    """

    if temp_var and any([f"_{quantity}" in variable for quantity in ["M", "PE", "PX", "PY", "PZ"]]): variable = f"temp_{variable}"

    if "exact" in category:
        if "abs" in category: return f"( ({variable} == {value}) || ({variable} == -{value}) )"
        elif "not" in category: return f"({variable} != {value})"
        return f"({variable} == {value})"
    elif category == "min": return f"({variable} > {value})"
    elif category == "max": return f"({variable} < {value})"
    elif category == "in_range": return f"( ({variable} > {value[0]}) && ({variable} < {value[1]}) )"
    elif category == "out_range": return f"( ({variable} < {value[0]}) || ({variable} > {value[1]}) )"
    raise ValueError(f"Invalid category {category} given")

def calculate_inv_mass(data, particles, temp_vars=[]):
    """
    Defines the specified two-body invariant mass within the dataset.

    Args:
        data: RDataFrame object containing the dataset to be selected.
        particles: the list of particles {1, 3-5} which the invarinat mass is to be constructed from.
        temp_vars: temporary variables which are to be removed before the writing of the dataset
    Returns:
        data: The RDataFrame object with the invariant mass of the particles calculated.
    """

    get_branch = lambda quantity, particles, temp_vars: [f"temp_lab{particle}_{quantity}" if any([particle in temp_var for temp_var in temp_vars]) else f"lab{particle}_{quantity}" for particle in particles]
    energies = get_branch("PE", particles, temp_vars)
    momenta_x = get_branch("PX", particles, [])
    momenta_y = get_branch("PY", particles, [])
    momenta_z = get_branch("PZ", particles, [])
    assert len(energies) > 0

    variable = f"temp_INV_{''.join(particles)}" if len(temp_vars) > 0 else f"INV_{''.join(particles)}"
    cut_string = f"sqrt(pow(({' + '.join(energies)}), 2) - pow(({' + '.join(momenta_x)}), 2) - pow(({' + '.join(momenta_y)}), 2) - pow(({' + '.join(momenta_z)}), 2))"
    if variable in data.GetColumnNames():
        return data.Redefine(variable, cut_string)
    return data.Define(variable, cut_string)

def define_inv_mass(data, variable, temp_vars=[]):
    """
    Defines the specified two-body invariant mass within the dataset.

    Args:
        data: RDataFrame object containing the dataset to be selected.
        variable: The name of the variable on which a cut is to be made, here INV_{34, 45, low, high}.
    Returns:
        data: The RDataFrame object with the invariant mass specified defined.
    """

    if "34" in variable or "high" in variable or "low" in variable:
        data = calculate_inv_mass(data, ["3","4"], temp_vars=temp_vars)
    if "45" in variable or "high" in variable or "low" in variable:
        data = calculate_inv_mass(data, ["4","5"], temp_vars=temp_vars)

    prefix = "temp_" if len(temp_vars) > 0 else ""
    if "high" in variable:
        # High is the highest of each pair of invariant masses, as per the Dalitz folding technique
        if variable in data.GetColumnNames():
            return data.Redefine(variable, f"max({prefix}INV_34, {prefix}INV_45)")
        return data.Define(variable, f"max({prefix}INV_34, {prefix}INV_45)")
    if "low" in variable:
        # Low is the lowest of each pair of invariant masses, as per the Dalitz folding technique
        if variable in data.GetColumnNames():
            return data.Redefine(variable, f"min({prefix}INV_34, {prefix}INV_45)")
        return data.Define(variable, f"min({prefix}INV_34, {prefix}INV_45)")
    return data

def get_cut(
    data,
    cuts_dict: Dict[str, str],
    temp_vars
    ):
    """
    """

    cuts = []
    for variable, cut in cuts_dict.items():
        if variable != "combination":
            if "INV" in variable:
                data = define_inv_mass(data, variable, temp_vars=temp_vars)
            cuts += [
                parse_cut(
                    variable,
                    cut["cut_value"],
                    cut["cut_type"],
                    temp_var=len(temp_vars)
                )
            ]
    
    return data, f" {'&&' if 'combination' in cuts_dict and cuts_dict['combination'] == 'AND' else '||'} ".join(cuts)

@njit(cache=True)
def get_mass(particle_ID):
    if absolute(particle_ID) == 211: return 139.570
    elif absolute(particle_ID) == 2212: return 938.272
    elif absolute(particle_ID) == 321: return 493.677
    return 0

def assign_mass_hypotheses(data, assign_set, is_temp=False):

    temp_vars = []
    prefix = "temp_" if is_temp else ""
    for variable, assign_particle in assign_set.items():
        var_num = variable.replace("lab","").split('_',1)[0]
        if is_temp:
            temp_vars.append(var_num)

        if f"{prefix}lab{var_num}_M" in data.GetColumnNames():
            data = data.Redefine(f"{prefix}lab{var_num}_M", str(get_mass(assign_particle)))
            data = data.Redefine(f"{prefix}lab{var_num}_PE", f"sqrt(pow({prefix}lab{var_num}_M,2) + pow(lab{var_num}_PX,2) + pow(lab{var_num}_PY,2) + pow(lab{var_num}_PZ,2))")
        else:
            data = data.Define(f"{prefix}lab{var_num}_M", str(get_mass(assign_particle)))
            data = data.Define(f"{prefix}lab{var_num}_PE", f"sqrt(pow({prefix}lab{var_num}_M,2) + pow(lab{var_num}_PX,2) + pow(lab{var_num}_PY,2) + pow(lab{var_num}_PZ,2))")

    if any([branch in assign_set for branch in ["lab3_ID", "lab4_ID", "lab5_ID"]]):
        temp_vars.append("2")
        for momentum in ["PE", "PX", "PY", "PZ"]:
            construction_P = ""
            for particle in ["3", "4", "5"]:
                construction_P += f"temp_lab{particle}_{momentum}" if particle in temp_vars and is_temp and momentum =="PE" else f"lab{particle}_{momentum}"
                if particle != "5": construction_P += " + "

            data = data.Redefine(f"{prefix}lab2_{momentum}", construction_P) if f"{prefix}lab2_{momentum}" in data.GetColumnNames()\
                 else data.Define(f"{prefix}lab2_{momentum}", construction_P)

        construction_M = f"sqrt(pow({prefix}lab2_PE, 2) - pow({prefix}lab2_PX,2) - pow({prefix}lab2_PY, 2) - pow({prefix}lab2_PZ, 2))"
        data = data.Redefine(f"{prefix}lab2_M", construction_M) if f"{prefix}lab2_M" in data.GetColumnNames()\
             else data.Define(f"{prefix}lab2_M", construction_M)

    if is_temp: temp_vars.append("0")
    for momentum in ["PE", "PX", "PY", "PZ"]:
        lab0_prefix = ""
        lab1_prefix = ""
        lab2_prefix = ""
        if is_temp:
            lab0_prefix += "temp_"
            if momentum == "PE":
                if "1" in temp_vars: lab1_prefix += "temp_"
                if "2" in temp_vars: lab2_prefix += "temp_"

        data = data.Redefine(f"{lab0_prefix}lab0_{momentum}", f"{lab1_prefix}lab1_{momentum} + {lab2_prefix}lab2_{momentum}") if f"{lab0_prefix}lab0_{momentum}" in data.GetColumnNames()\
             else data.Define(f"{lab0_prefix}lab0_{momentum}", f"{lab1_prefix}lab1_{momentum} + {lab2_prefix}lab2_{momentum}")

    construction_M = f"sqrt(pow({prefix}lab0_PE, 2) - pow({prefix}lab0_PX,2) - pow({prefix}lab0_PY, 2) - pow({prefix}lab0_PZ, 2))"
    return data.Redefine(f"{prefix}lab0_M", construction_M) if f"{prefix}lab0_M" in data.GetColumnNames()\
                                                            else data.Define(f"{prefix}lab0_M", construction_M),\
           temp_vars

def apply_cut(
    data,
    selection:  dict,
    temp_vars
    ):
    """
    """

    data, cut_string = get_cut(data, selection["cuts"], temp_vars)
    
    if "conditions" in selection:
        data, conditions = get_cut(data, selection["conditions"], temp_vars)
        cut_string = f"!( {conditions} ) || ( {cut_string} )"

    if "iteration" in selection:
        strings = [cut_string.replace(selection["iteration"]["iteration_var"], replacement) for replacement in selection["iteration"]["replacement"]]
        cut_string = f"( {' ) && ( '.join(strings)} )"

    return data.Filter(cut_string)

def apply_selection(data, selection):
    """
    """
    
    temp_vars = []
    if "mass_hypotheses" in selection:
        data, temp_vars = assign_mass_hypotheses(data, selection["mass_hypotheses"], is_temp = len(selection.keys()) > 1)
    if "cuts" in selection:
        return apply_cut(
            data,
            selection,
            temp_vars=temp_vars
        )
    return data