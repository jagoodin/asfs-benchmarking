###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
"""

import numpy as np
import numba as nb
from awkward import Array
from typing import Dict, List, Union
from time import monotonic_ns
#@njit
def parse_cut(
    data,
    variable: str,
    value:    Union[List[float], float, bool],
    category: str,
    temp_var: bool=False
    ):
    """
    """

    if "exact" in category:
        if "abs" in category: return abs(data[variable]) == value
        elif "not" in category: return data[variable] != value
        return data[variable] == value
    elif category == "min": return data[variable] > value
    elif category == "max": return data[variable] < value
    elif category == "in_range": return (data[variable] > float(value[0])) & (data[variable] < float(value[1]))
    elif category == "out_range": return (data[variable] < float(value[0])) | (data[variable] > float(value[1]))
    raise ValueError(f"Invalid category {category} given")


def recursive_and(
    indices,
    idx=[]
    ):
    """
    """
    
    if len(indices) > 0:
        return recursive_and(
            indices[1:],
            idx & indices[0] if len(idx) > 0 else indices[0]
        )
    return idx

def recursive_or(
    indices,
    idx=[]
    ):
    """
    """
    
    if len(indices) > 0:
        return recursive_or(
            indices[1:],
            idx | indices[0] if len(idx) > 0 else indices[0]
        )
    return idx

@nb.njit(parallel=True, fastmath=True)
def _compute_mass(ens, pxs, pys, pzs):
    # Credit to Hans Dembinski for 
    n = len(ens[0]) if len(ens) > 0 else 0
    result = np.zeros((n, 4))
    for i in nb.prange(n):
        for en in ens:
            result[i, 0] += en[i]
        for px in pxs:
            result[i, 1] += px[i]
        for py in pys:
            result[i, 2] += py[i]
        for pz in pzs:
            result[i, 3] += pz[i]
    result[:, 0] = result[:, 0] ** 2 - result[:, 1] ** 2 - result[:, 2] ** 2 - result[:,3] ** 2
    return result[:, 0] ** 0.5

def calculate_inv_mass(data, particles, temp_vars=[]):
    """
    Defines the specified two-body invariant mass within the dataset.

    Args:
        data: RDataFrame object containing the dataset to be selected.
        particles: the list of particles {1, 3-5} which the invarinat mass is to be constructed from.
        temp_vars: temporary variables which are to be removed before the writing of the dataset
    Returns:
        data: The RDataFrame object with the invariant mass of the particles calculated.
    """
    # Credit to Hans Dembinski for reworking the computation of invariant masses in an efficient and parallelisable manner

    get_branch = lambda quantity, particles, temp_vars: [f"temp_lab{particle}_{quantity}" if any([particle in temp_var for temp_var in temp_vars]) else f"lab{particle}_{quantity}" for particle in particles]
    energies = get_branch("PE", particles, temp_vars)
    momenta_x = get_branch("PX", particles, [])
    momenta_y = get_branch("PY", particles, [])
    momenta_z = get_branch("PZ", particles, [])
    assert len(energies) > 0

    key = f"temp_INV_{''.join(particles)}" if len(temp_vars) > 0 else f"INV_{''.join(particles)}"

    data[key] = _compute_mass(tuple(data[pe] for pe in energies), 
                              tuple(data[px] for px in momenta_x),
                              tuple(data[py] for py in momenta_y),
                              tuple(data[pz] for pz in momenta_z))

    return data

def define_inv_mass(data, variable, temp_vars=[]):
    """
    Defines the specified two-body invariant mass within the dataset.

    Args:
        data: RDataFrame object containing the dataset to be selected.
        variable: The name of the variable on which a cut is to be made, here INV_{34, 45, low, high}.
    Returns:
        data: The RDataFrame object with the invariant mass specified defined.
    """

    if "34" in variable or "high" in variable or "low" in variable:
        data = calculate_inv_mass(data, ["3","4"], temp_vars=temp_vars)
    if "45" in variable or "high" in variable or "low" in variable:
        data = calculate_inv_mass(data, ["4","5"], temp_vars=temp_vars)

    prefix = "temp_" if len(temp_vars) > 0 else ""
    if "high" in variable:
        # High is the highest of each pair of invariant masses, as per the Dalitz folding technique
        data[variable] = Array(map(max, data[f"{prefix}INV_34"], data[f"{prefix}INV_45"]))
    if "low" in variable:
        # Low is the lowest of each pair of invariant masses, as per the Dalitz folding technique
        data[variable] = Array(map(min, data[f"{prefix}INV_34"], data[f"{prefix}INV_45"]))
    return data

def get_cut(
    data,
    cuts_dict:  Dict[str, str],
    temp_vars,
    iter_key:   str = "",
    iter_value: str = ""
    ):
    """
    """
    cuts = []
    for variable, cut in cuts_dict.items():
        if variable != "combination":
            if "INV" in variable:
                data = define_inv_mass(data, variable, temp_vars=temp_vars)
            cuts += [
                parse_cut(
                    data,
                    variable.replace(iter_key, iter_value) if iter_key and iter_value else variable,
                    cut["cut_value"],
                    cut["cut_type"],
                    temp_var=len(temp_vars)
                )
            ]

    return data, recursive_and(cuts) if "combination" in cuts_dict and cuts_dict["combination"] == "AND" else recursive_or(cuts)

@nb.njit(cache=True)
def get_mass(particle_ID):
    if np.abs(particle_ID) == 211: return 139.570
    elif np.abs(particle_ID) == 2212: return 938.272
    elif np.abs(particle_ID) == 321: return 493.677
    return 0

def assign_mass_hypotheses(
    data,
    assign_set,
    is_temp=False
    ):

    temp_vars = []
    prefix = "temp_" if is_temp else ""
    for variable, assign_particle in assign_set.items():
        var_num = variable.replace("lab","").split('_',1)[0]
        if is_temp:
            temp_vars.append(var_num)

        data[f"{prefix}lab{var_num}_M"] = get_mass(assign_particle)
        data[f"{prefix}lab{var_num}_PE"] = (data[f"{prefix}lab{var_num}_M"]**2 + data[f"lab{var_num}_PX"]**2 + data[f"lab{var_num}_PY"]**2 + data[f"lab{var_num}_PZ"]**2) ** 0.5

    if any([branch in assign_set for branch in ["lab3_ID", "lab4_ID", "lab5_ID"]]):
        temp_vars.append("2")
        for momentum in ["PE", "PX", "PY", "PZ"]:
            data[f"{prefix}lab2_{momentum}"] = data[f"{'temp_' if '3' in temp_vars and momentum == 'PE' else ''}lab3_{momentum}"] + data[f"{'temp_' if '4' in temp_vars and momentum == 'PE' else ''}lab4_{momentum}"] + data[f"{'temp_' if '5' in temp_vars and momentum == 'PE' else ''}lab5_{momentum}"]

        data[f"{prefix}lab2_M"] = (data[f"{prefix}lab2_PE"]**2 - data[f"{prefix}lab2_PX"]**2 - data[f"{prefix}lab2_PY"]**2 - data[f"{prefix}lab2_PZ"]**2) ** 0.5

    if is_temp: temp_vars.append("0")
    for momentum in ["PE", "PX", "PY", "PZ"]:
        lab0_prefix = ""
        lab1_prefix = ""
        lab2_prefix = ""
        if is_temp:
            lab0_prefix += "temp_"
            if momentum == "PE":
                if "1" in temp_vars: lab1_prefix += "temp_"
                if "2" in temp_vars: lab2_prefix += "temp_"

        data[f"{lab0_prefix}lab0_{momentum}"] = data[f"{lab1_prefix}lab1_{momentum}"] + data[f"{lab2_prefix}lab2_{momentum}"]

    data[f"{prefix}lab0_M"] = (data[f"{prefix}lab0_PE"]**2 + data[f"{prefix}lab0_PX"]**2 + data[f"{prefix}lab0_PY"]**2 + data[f"{prefix}lab0_PZ"]**2) ** 0.5
    return data, temp_vars

def apply_cut(
    data,
    selection,
    temp_vars
    ):
    """
    """

    if "iteration" in selection:
        iter_key = selection["iteration"]["iteration_var"]
        iter_cut_indices = []
        for i, iter_val in enumerate(selection["iteration"]["replacement"]):
            data, iter_cut_index = get_cut(data, selection["cuts"], temp_vars, iter_key, iter_val)
            iter_cut_indices += [iter_cut_index]
            if "conditions" in selection:
                data, conditions = get_cut(data, selection["conditions"], temp_vars, iter_key, iter_val)
                iter_cut_indices[i] = ~conditions | iter_cut_indices[i]
        cut_indices = recursive_and(iter_cut_indices)
    else:
        data, cut_indices = get_cut(data, selection["cuts"], temp_vars)

        if "conditions" in selection:
            data, conditions = get_cut(data, selection["conditions"], temp_vars)
            cut_indices = ~conditions | cut_indices
    return data[cut_indices]

def apply_selection(data, selection):
    """
    """

    temp_vars = []
    if "mass_hypotheses" in selection:
        data, temp_vars = assign_mass_hypotheses(data, selection["mass_hypotheses"], is_temp = len(selection.keys()) > 1)
    if "cuts" in selection:
        return apply_cut(
            data,
            selection,
            temp_vars=temp_vars
        )
    return data