###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
"""

import argparse
import datetime as dt
import functions as f
import os
import uproot

from functools import partial
from implementations import rdataframe as rdf, uproot as upr

from ROOT import EnableImplicitMT, RDataFrame
#from ROOT.RDF import RunGraphs

parser = argparse.ArgumentParser()

parser.add_argument("--input", action = "store", nargs="+", type = str, required = True)
parser.add_argument("--data", action = "store", type = str, required = True)
parser.add_argument("--output", action = "store", type = str, required = True)

parser.add_argument("--branches", action = "store", nargs="+", type = str, required = True)
parser.add_argument("--config", action = "store", nargs="+", type = str, required = True)

parser.add_argument("--cores", action = "store", type = int, required = True)

parser.add_argument("--rdataframe", action='store_true')
parser.add_argument("--uproot", action='store_true')

parser.add_argument("--verbose", action='store_true')

args = parser.parse_args()

assert args.rdataframe != args.uproot, AssertionError("One of --rdataframe or --uproot must be passed when running benchmark.py")

branches = f.get_branches(args.branches)
selections = f.load_yamls(args.config)
decay_tree = "Bs2DsPiOfflineTree/DecayTree"

if args.verbose:
    print("Branches enabled:")
    for branch in branches:
        print(f" -- {branch}")
    print("\nSelections enabled:")
    for selection in selections:
        print(f" -- {selection}")
    print()

start = dt.datetime.now()
if args.rdataframe:
    # Running RDataFrame form of benchmarking
    EnableImplicitMT(args.cores)
    data = RDataFrame(decay_tree, args.input, branches)

    for label, selection in selections.items():
        data = rdf.apply_selection(data, selection)
    print(f"Writing to {args.data}")
    data.Snapshot(decay_tree, args.data, branches)

else:
    # Running RDataFrame form of benchmarking
    with uproot.recreate(args.data) as outtuple:
        for chunk in uproot.iterate(
            [f"{infile}:{decay_tree}" for infile in args.input],
            branches,
            num_workers=args.cores,
            step_size=16384
        ):
            for selection in selections.values():
                chunk = upr.apply_selection(chunk, selection)

            if "decay_tree" in outtuple.keys():
                outtuple[decay_tree].extend(chunk)
            else:
                outtuple[decay_tree] = chunk           

stop = dt.datetime.now()

events = 0
file_size = 0
for file_path in args.input:
    with uproot.open(f"{file_path}:{decay_tree}") as infile:
        events+= len(infile["lab0_M"].array())
    file_size += os.path.getsize(file_path)

if args.verbose:
    print("Timing information\n")
    print(f"{'Start:':<16}{start}")
    print(f"{'Stop:':<16}{stop}\n")
    print(f"{'Difference:':<16}{str(stop-start)}")
    print(f"{'Events:':<16}{events}")
    print(f"{'Files size:':<16}{file_size}")

with open(args.output, 'w') as outfile:
    outfile.write("Timing information\n")
    outfile.write("------------------\n")
    outfile.write(f"{'Start:':<16}{start}\n")
    outfile.write(f"{'Stop:':<16}{stop}\n")
    outfile.write(f"{'Events:':<16}{events}\n")
    outfile.write(f"{'Files size:':<16}{file_size}\n")
    outfile.write(f"{'Difference:':<16}{str(stop-start)}\n")