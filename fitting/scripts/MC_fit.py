import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--data", action = "store", dest = "data", type = str, required = True)
parser.add_argument("--config", action = "store", dest = "config", type = str, required = True)
parser.add_argument("--shape", action = "store", dest = "shape", type = str, required = True)
parser.add_argument("--output", action = "store", dest = "output", type = str, required = True)


parser.add_argument("--bins", action = "store", dest = "bins", type = int, required = True)
parser.add_argument("--cores", action = "store", dest = "cores", type = int, required = True)
parser.add_argument("--trial", action = "store", dest = "trial", type = int, required = False)

args = parser.parse_args()

import json
import numpy as np
import os
import ROOT as R
import time

#R.gROOT.SetBatch(True)
R.gInterpreter.ProcessLine('#include "{0}"'.format(args.shape.replace("_cxx.so", ".h")))
R.gSystem.Load(args.shape)

def construct_cruijff(ws, label, extended=True, form="New", inv_mass="lab0_M"):

    ws.factory(f"{label}_mean[5368,5350,5380]")
    ws.factory(f"{label}_sigma_l[15,5,30]")
    ws.factory(f"{label}_sigma_r[15,5,30]")

    if extended:
        ws.factory(f"{form}ExtCruijff::{label}_pdf({inv_mass}, {label}_mean, {label}_sigma_l, {label}_sigma_r, {label}_alpha_l[1, 0, 10], {label}_alpha_r[1, 0, 10], {label}_beta[0, -0.001, 0.001])")
    else:
        ws.factory(f"{form}Cruijff::{label}_pdf({inv_mass}, {label}_mean, {label}_sigma_l, {label}_sigma_r, {label}_alpha_l[1, 0, 10], {label}_alpha_r[1, 0, 10])")
    return ws

with open(args.config, 'r') as config_file:
    config = json.load(config_file)

file = R.TFile.Open(args.data, 'READ')
tree = file.Get(config["decay_tree"])
tree.SetBranchStatus("*",0)

tree.SetBranchStatus("lab0_M",1)
tree.SetBranchStatus("lab2_M",1)
lab0_M = R.RooRealVar('lab0_M', config['B_channel_label'], config['B_mass'][0], config['B_mass'][1], 'MeV/#it{c}^{2}')
lab2_M = R.RooRealVar('lab2_M', config['D_channel_label'], config['D_mass'][0], config['D_mass'][1], 'MeV/#it{c}^{2}')

if config["mode"] == "MC":
    if args.bins > 0:
        temp_data = R.RooDataSet('temp_data', 'temp_data', tree, R.RooArgSet(lab0_M, lab2_M))  
        if "B" in config["meson"]: lab0_M.setBins(args.bins)
        if "D" in config["meson"]: lab2_M.setBins(args.bins)
        data = R.RooDataHist('data', 'data', R.RooArgSet(lab0_M, lab2_M), temp_data)
    else:
        data = R.RooDataSet('data', 'data', tree, R.RooArgSet(lab0_M, lab2_M))
    data.Print()
else:
    quit


ws = R.RooWorkspace("ws")
ws.Import(data)

if config["mode"] == "MC":
    B_label = "signal" if "Bs2DsPi" in config['B_channel'] else config['B_channel']
    pdf_label = f'{B_label}_{D_channel}' if "D" in config["meson"] else B_label
    construct_cruijff(ws, pdf_label,
                      extended = "Ext" in args.shape.rsplit('/',1)[1],
                      form = "New" if "New" in args.shape.rsplit('/',1)[1] else "Old",
                      inv_mass = "lab2_M" if "D" in config["meson"] else "lab0_M")
    pdf = ws.pdf(f"{pdf_label}_pdf")
else:
    quit

if args.trial: R.RooRandom.randomGenerator().SetSeed(args.trial)
for param in pdf.getParameters(data):
    param.randomize()

start = time.time()
fit_result = pdf.fitTo(data, Save=True, NumCPU=args.cores, Minimizer=["Minuit2", "migrad"], ConditionalObservables=[lab2_M,], Optimize=False, BatchMode=True)
stop = time.time()

chi2_bins = 100
variable = lab0_M if "B" in config["meson"] else lab2_M
Ndof = chi2_bins - len([param for param in pdf.getParameters(data) if not(param.isConstant())])
th1_data = data.createHistogram("data", variable, R.RooFit.Binning(chi2_bins, variable.getMin(), variable.getMax()))
binned_data = R.RooDataHist(f"binned_data", f"binned_data", R.RooArgList(variable,), th1_data)

chi2_var = R.RooChi2Var("chi2", "chi2", pdf, binned_data)
chi2 = chi2_var.getVal()
chi2_error = chi2_var.getPropagatedError(fit_result)
rchi2 = chi2/Ndof
rchi2_error = chi2/Ndof

with open(args.output, "w") as output_file:
    for param in fit_result.floatParsFinal():
        output_file.write(f"{param.GetName():<24} = {param.getVal():<24} +/- {param.getError():<24}\n")
    output_file.write("\n")
    output_file.write(f"Time elapsed: {stop-start} s\n")
    output_file.write(f"Covariance quality: {fit_result.covQual()}\n")
    output_file.write(f"Minuit status: {fit_result.status()}\n")
    output_file.write(f"Chi^2: {chi2} +/- {chi2_error}\n")
    output_file.write(f"Reduced chi^2: {rchi2} +/- {rchi2_error}\n")
    if args.bins > 0:
        output_file.write(f"Bins: {variable.getBins()}")