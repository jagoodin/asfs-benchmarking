###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
"""

import yaml

from pathlib import Path
from typing import List

def load_yamls(
    paths: List[str]
    ):
    """
    """
    yamls = {}
    for path in paths:
        new_yaml = yaml.safe_load(Path(path).read_text())
        yamls.update(
            {
                item: yamls[item] + values if item in yamls else values for item, values in new_yaml.items()
            }
        )
    return yamls

def get_branches(branch_files: List[str]):
    """
    """
    branches = load_yamls(branch_files)["branches"]

    return branches