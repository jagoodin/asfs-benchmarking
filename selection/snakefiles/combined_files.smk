localrules: combine_files

rule combine_files:
    input:
        data = lambda wildcards: [f"/scratch/jgooding/B2OC/benchmarking/inputs/00173396_0000000{n+1}_1.bs2dspi.root" for n in range(int(wildcards.num_files))]
    threads:1
    output:
        data = "/scratch/jgooding/B2OC/benchmarking/inputs/files{num_files}_combined.root"
    shell:
        "hadd -fk {output.data} {input.data}"


rule run_benchmark_combined_1cores:
    input:
        script = "scripts/benchmark.py",
        data = "/scratch/jgooding/B2OC/benchmarking/inputs/files{num_files}_combined.root",
        config = get_configs,
        branches = get_branches
    threads: 1
    wildcard_constraints:
        tool = "(rdataframe|uproot)",
        config = "(simple|complex)"
    output:
        data = temp("ntuples/{tool}_combined/{config}/files-{num_files}/cores-1/{config}_files-{num_files}_cores-1_trial-{trial}.root"),
        results = "results/{tool}_combined/{config}/files-{num_files}/cores-1/{config}_files-{num_files}_cores-1_trial-{trial}.txt"
    shell:
        """
        python {input.script} --input {input.data} --data {output.data} --output {output.results} --branches {input.branches} --config {input.config} --{wildcards.tool} --cores 1 --verbose
        """

rule run_benchmark_combined_2cores:
    input:
        script = "scripts/benchmark.py",
        data = "/scratch/jgooding/B2OC/benchmarking/inputs/files{num_files}_combined.root",
        config = get_configs,
        branches = get_branches
    threads: 2
    wildcard_constraints:
        tool = "(rdataframe|uproot)",
        config = "(simple|complex)"
    output:
        data = temp("ntuples/{tool}_combined/{config}/files-{num_files}/cores-2/{config}_files-{num_files}_cores-2_trial-{trial}.root"),
        results = "results/{tool}_combined/{config}/files-{num_files}/cores-2/{config}_files-{num_files}_cores-2_trial-{trial}.txt"
    shell:
        """
        python {input.script} --input {input.data} --data {output.data} --output {output.results} --branches {input.branches} --config {input.config} --{wildcards.tool} --cores 2 --verbose
        """

rule run_benchmark_combined_4cores:
    input:
        script = "scripts/benchmark.py",
        data = "/scratch/jgooding/B2OC/benchmarking/inputs/files{num_files}_combined.root",
        config = get_configs,
        branches = get_branches
    threads: 4
    wildcard_constraints:
        tool = "(rdataframe|uproot)",
        config = "(simple|complex)"
    output:
        data = temp("ntuples/{tool}_combined/{config}/files-{num_files}/cores-4/{config}_files-{num_files}_cores-4_trial-{trial}.root"),
        results = "results/{tool}_combined/{config}/files-{num_files}/cores-4/{config}_files-{num_files}_cores-4_trial-{trial}.txt"
    shell:
        """
        python {input.script} --input {input.data} --data {output.data} --output {output.results} --branches {input.branches} --config {input.config} --{wildcards.tool} --cores 4 --verbose
        """

rule run_benchmark_combined_8cores:
    input:
        script = "scripts/benchmark.py",
        data = "/scratch/jgooding/B2OC/benchmarking/inputs/files{num_files}_combined.root",
        config = get_configs,
        branches = get_branches
    threads: 8
    wildcard_constraints:
        tool = "(rdataframe|uproot)",
        config = "(simple|complex)"
    output:
        data = temp("ntuples/{tool}_combined/{config}/files-{num_files}/cores-8/{config}_files-{num_files}_cores-8_trial-{trial}.root"),
        results = "results/{tool}_combined/{config}/files-{num_files}/cores-8/{config}_files-{num_files}_cores-8_trial-{trial}.txt"
    shell:
        """
        python {input.script} --input {input.data} --data {output.data} --output {output.results} --branches {input.branches} --config {input.config} --{wildcards.tool} --cores 8 --verbose
        """