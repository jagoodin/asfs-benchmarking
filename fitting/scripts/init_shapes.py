import ROOT as R

import argparse
import numpy as np
import os

parser = argparse.ArgumentParser()
parser.add_argument("--output", action = "store", dest = "output", type = str, required = True)

args = parser.parse_args()

if not os.path.exists(args.output):
    os.mkdir(args.output)
os.chdir(args.output) 

x = R.RooRealVar("x", "x", -np.inf, np.inf)
mu = R.RooRealVar("mu", "mu", -np.inf, np.inf)
sigma_l = R.RooRealVar("sigma_l", "sigma_l", 0, np.inf)
sigma_r = R.RooRealVar("sigma_r", "sigma_r", 0, np.inf)
alpha_l = R.RooRealVar("alpha_l", "alpha_l", 0, np.inf)
alpha_r = R.RooRealVar("alpha_r", "alpha_r", 0, np.inf)


cruijff_vars = R.RooArgList(x, mu, sigma_l, sigma_r, alpha_l, alpha_r)
R.RooClassFactory.makeAndCompilePdf("OldCruijff",
                                    "(x < mu) * exp(-pow(x - mu, 2) / (2 * pow(sigma_l, 2) + alpha_l * pow(x - mu, 2))) + " + \
                                    "(x >= mu) * exp(-pow(x - mu, 2) / (2 * pow(sigma_r, 2) + alpha_r * pow(x - mu, 2)));",
                                    cruijff_vars)
R.RooClassFactory.makeAndCompilePdf("NewCruijff",
                                    "exp(-(x - mu)*(x - mu) / (" + \
                                    "x < mu ? (2 * sigma_l*sigma_l + alpha_l * (x - mu)*(x - mu)) : " + \
                                    "(2 * sigma_r*sigma_r + alpha_r * (x - mu)*(x - mu))" + \
                                    "));",
                                    cruijff_vars)

beta = R.RooRealVar("beta", "beta", -1, 1)
ext_cruijff_vars = R.RooArgList(x, mu, sigma_l, sigma_r, alpha_l, alpha_r, beta)
R.RooClassFactory.makeAndCompilePdf("OldExtCruijff",
                                    "(x < mu) * exp(-pow(x - mu, 2) * pow(1 + beta * (x - mu), 2) / (2 * pow(sigma_l, 2) + alpha_l * pow(x - mu, 2))) + " + \
                                    "(x >= mu) * exp(-pow(x - mu, 2)  * pow(1 + beta * (x - mu), 2) / (2 * pow(sigma_r, 2) + alpha_r * pow(x - mu, 2)));",
                                    ext_cruijff_vars)
R.RooClassFactory.makeAndCompilePdf("NewExtCruijff",
                                    "exp(-(x - mu)*(x - mu) * (1 + beta * (x - mu))*(1 + beta * (x - mu)) / (" + \
                                    "x < mu ? (2 * sigma_l*sigma_l + alpha_l * (x - mu)*(x - mu)) :" + \
                                    "(2 * sigma_r*sigma_r + alpha_r * (x - mu)*(x - mu))" + \
                                    "));",
                                    ext_cruijff_vars)