import argparse
import datetime as dt
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib
matplotlib.rc_file('../repos/lhcb-matplotlibrc/matplotlibrc')

parser = argparse.ArgumentParser()

parser.add_argument("--input", action = "store", type = str, nargs="+", required = True)
parser.add_argument("--config", action = "store", type = str, required = True)
parser.add_argument("--output", action = "store", type = str, nargs="+", required = True)

args = parser.parse_args()

def get_time(path):
    with open(path, 'r') as infile:
        for line in infile:
            if "Difference" in line:
                return line.strip().split("Difference:",1)[1]

def get_size(path):
    with open(path, 'r') as infile:
        for line in infile:
            if "Files size" in line:
                return int(line.strip().split("Files size:",1)[1]) / (1024 * 1024 * 1024)

def get_events(path):
    with open(path, 'r') as infile:
        for line in infile:
            if "Events" in line:
                return int(line.strip().split("Events:",1)[1]) / 1e6




time_format = '%M:%S.%f'

dfs = []
for n, file in enumerate(args.input):
    tool = file.split("/", 2)[1]
    time = get_time(file)
    size = get_size(file)
    events = get_events(file)
    _, file = file.replace(".txt","").replace("_","-").rsplit("/", 1)
    config, _, num_files, _, cores, _, trial = file.split("-")
    dfs += [
        pd.DataFrame.from_dict(
            {str(n): [tool, config, int(num_files), int(cores), trial, dt.datetime.strptime(time.split(":",1)[1], time_format), float(size), float(events)]},
            orient="index",
            columns=["tool", "config", "num_files", "cores", "trial", "time", "size", "events"]
        )   
    ]
total_df = pd.concat(dfs)
uproot_df = total_df[(total_df.tool == "uproot") | (total_df.tool == "uproot_combined")]
rdataframe_df = total_df[(total_df.tool == "rdataframe") | (total_df.tool == "rdataframe_combined")]

def plot(uproot_df, rdataframe_df, denominator=""):



    fig, axs = plt.subplots(2, 2, figsize=(18,10), sharex=True, sharey=True)
    plt.subplots_adjust(wspace=0, hspace=0)
    for df, tool_label, marker, color in zip((uproot_df, rdataframe_df), ("uproot5+numba", "RDataFrame"), ("o", "^"), ("#22AADD", "#043678")):
        for ax, num_files, linestyle in zip(axs.flatten(), sorted(pd.unique(df.num_files)), ("solid", "dashed", "dotted", "dashdot")):
            xvals = []
            yvals = []
            yerrs = []
            data_size = df[(df.config == args.config) & (df.num_files == num_files) & (df.cores == 1)]["size"][0]
            events = df[(df.config == args.config) & (df.num_files == num_files) & (df.cores == 1)]["events"][0]
            for cores in sorted(pd.unique(df.cores)):
                temp_df = df[(df.config == args.config) & (df.num_files == num_files) & (df.cores == cores)]
                dt_times = temp_df["time"]
                times = [60 * dt_time.minute + dt_time.second + 1e-6 * dt_time.microsecond for dt_time in dt_times]
                if denominator == "GB":
                    denom = data_size
                elif denominator == "file":
                    denom = num_files
                elif denominator == "event":
                    denom = events
                else:
                    denom = 1
                xvals += [cores]
                yvals += [np.mean(times)/denom]
                yerrs += [np.std(times)/denom]
            ax.errorbar(xvals, yvals, yerrs, label=tool_label if num_files == 1 else "",
                        marker=marker, ls=linestyle, color=color)
            if tool_label == "RDataFrame":
                ax.annotate(f"Selection: {args.config}", (0.025, 0.9), xycoords="axes fraction", ha="left", size=24)
                ax.annotate(f"Files: {num_files}", (0.95, 0.9), xycoords="axes fraction", ha="right", size=24)
                ax.annotate(f"Data size: {data_size:.2f} GB", (0.95, 0.8), xycoords="axes fraction", ha="right", size=24)
                ax.annotate(f"Events: {events:.2f} " + r"$\times$ 10${}^6$", (0.95, 0.7), xycoords="axes fraction", ha="right", size=24)
            ax.tick_params(axis='x', labelsize=24)
            ax.tick_params(axis='y', labelsize=24)
            ax.set_xlim(0.5, 8.5)
            ax.set_yscale("log")
        [ax.set_xlabel("Number of cores", size=24) for ax in axs[1]]


        if denominator == "GB":
            [ax.set_ylabel(r"Runtime / s GB${}^{-1}$", size=24) for ax in axs[:,0]]
        elif denominator == "file":
            [ax.set_ylabel(r"Runtime / s file${}^{-1}$", size=24) for ax in axs[:,0]]
        elif denominator == "event":
            [ax.set_ylabel(r"Runtime / s (10${}^{6}$ events)${}^{-1}$", size=24) for ax in axs[:,0]]
        else:
            [ax.set_ylabel(r"Runtime / s", size=24) for ax in axs[:,0]]

        ylims = axs[-1,-1].get_ylim()
        axs[-1,-1].set_ylim(ylims[0]/3, ylims[1]*3)
    fig.legend(loc='lower center', bbox_to_anchor=(0.325, 0.875),
                ncol=2, fancybox=True)
    fig.text(0.725, 0.915, 'LHCb Preliminary', {'size': 28})
    fig.text(0.585, 0.915, f'{max(total_df.trial.astype(int)) + 1} trials', {'size': 28})

    for output in args.output:
        if denominator: output = output.replace(".", f"_per_{denominator}.")
        plt.savefig(output)

#print("per GB")
plot(uproot_df, rdataframe_df, "GB")
#print("per file")
plot(uproot_df, rdataframe_df, "file")
#print("per event")
plot(uproot_df, rdataframe_df, "event")
#print("raw")
plot(uproot_df, rdataframe_df, "")